# README #

This is a repository for the development of an abstract agent-based model of settlement patterns.

### What is this repository for? ###

* This repo is for development purposes
* Changes are discussed in person and implemented by FH

### How do I get set up? ###

To access the latest version, go to source (in the left panel), and download the latest .nlogo file

### Who do I talk to? ###

* fabian held [at] sydney edu au
* Other community or team contact